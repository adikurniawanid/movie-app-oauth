# Movie App

Dari aplikasi movie yang sudah kalian kerjakan, buatlah CMS (Content Management System) di mana dapat melakukan proses CRUD terhadap movie yang akan ditayangkan.

## Ketentuan Aplikasi

- Terdapat fitur register
- Terdapat fitur login
  Fitur login harus menggunakan strategi JWT. Login dapat dilakukan dengan memasukkan email/username dan password serta google dan facebook OAuth
- User dapat melihat daftar film yang akan ditayangkan tanpa harus login
- User dapat menambahkan film mereka ke dalam daftar wishlist (harus login)
- User dapat menampilkan daftar wishlist mereka (harus login)
- User dapat menghapus film dari wishlist mereka (harus login)

## Ketentuan CMS

- Terdapat fitur login admin
  Fitur login harus menggunakan strategi JWT. Login hanya dapat dilakukan dengan memasukkan email/username dan password
- Terdapat fitur create admin (hanya dapat dilakukan oleh admin yang memiliki role superadmin)
- Terdapat fitur CRUD movie (bisa dilakukan oleh semua admin)

## Lainnya

- 1 email tidak boleh dipakai untuk menjadi admin CMS dan user sekaligus, misal: jika email adi@mail.com sudah dipakai untuk mendaftar sebagai user, maka tidak boleh dipakai sebagai admin CMS
- Semua kredensial (google client ID, google client secret, JWT secret WAJIB dimasukkan ke dalam .env)
- Admin CMS dan user boleh dimasukkan ke tabel yang sama (dengan role yang berbeda) atau terpisah
- Admin CMS memiliki 2 macam role, yaitu superadmin dan admin biasa
- Field semua tabel dibebaskan

## Ketentuan Pengerjaan

- Push kodingan ke repository movie-app kalian
- Tidak perlu menggunakan testing
- Deadline 19 Mei 2022, pukul 19.00
